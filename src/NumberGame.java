    import java.util.Scanner;

public class NumberGame 
{

	public static void main(String[] args) 
	{

		int totalMarks = 0;

		char gameStatus = 'A';

		System.out.println("Please press 'Y' to start the game, else press 'N' to quit\n");

		System.out.println("Please press 'q' if you want to quit the game at any point\n");

		System.out.println("Please remember there is a negative of 10 marks for a wrong answer "
				+ "and 10 marks added for the right answer");

		Scanner input = new Scanner(System.in);
		String response = input.next();
		if (response.equalsIgnoreCase("q"))
		{
			System.out.println("Ok! Take care!! Bye Bye.");
			System.exit(1);
		}
		System.out.println("Choose an operation amongst the following.\n" + "1. Addition\n" + "2. Subtraction\n"
				+ "3. Multiplication\n" + "4. Division\n");

		int operationType = input.nextInt();
		System.out.println(
				"Choose the game level.\n" + "1. Easy (1-10)\n" + "2. Medium (10-100)\n" + "3. Hard (100-1000)\n");
		int gameLevel = input.nextInt();

		int randomNumberRoundValue = 0;
//		int randomNumberRoundValue = gameLevel == 1 ? 10 : gameLevel == 2 ? 100 : 1000;

		if (gameLevel == 1) 
		{
			randomNumberRoundValue = 10;
		}
		else if (gameLevel == 2)
		{
			randomNumberRoundValue = 100;
		} 
		else if (gameLevel == 3) 
		{
			randomNumberRoundValue = 1000;
		}

		switch (operationType) 
		{ 
		case 1:  // 1--> Addition
			while (gameStatus != 'q')
			{
				int q1 = (int) (Math.random() * randomNumberRoundValue);
				int q2 = (int) (Math.random() * randomNumberRoundValue);
				System.out.println("What is " + q1 + " + " + q2);
				int answer = input.nextInt();
				if (answer == (q1 + q2)) 
				{
					System.out.println("Awesome!. You made it great!!");
					totalMarks += 10;
				} 
				else 
				{
					System.out.println("Sorry, wrong answer. Better luck for next question");
					totalMarks -= 10;
				}
				System.out.println("Press 'Y' for next question, else press 'q' to exit");
				String updatedGameStatus = input.next();
				gameStatus = updatedGameStatus.charAt(0);  // question // characterAt(2)=e
			}
			System.out.println("Total Marks are : " + totalMarks);
			break;

		case 2:   // 2--> Subtraction
			while (gameStatus != 'q') 
			{
				int q1 = (int) (Math.random() * randomNumberRoundValue);
				int q2 = (int) (Math.random() * randomNumberRoundValue);
				System.out.println("What is " + q1 + " - " + q2);
				int answer = input.nextInt();
				if (answer == (q1 - q2)) 
				{
					System.out.println("Awesome!. You made it great!!");
					totalMarks += 10;
				} 
				else
				{
					System.out.println("Sorry, wrong answer. Better luck for next question");
					totalMarks -= 10;
				}
				System.out.println("Press 'Y' for next question, else press 'q' to exit");
				String updatedGameStatus = input.next();
				gameStatus = updatedGameStatus.charAt(0);  // question // characterAt(2)=e
			}
			System.out.println("Total Marks are : " + totalMarks);
			

			break;

		case 3:  // 3--> Multiplication
			while (gameStatus != 'q') 
			{
				int q1 = (int) (Math.random() * randomNumberRoundValue);
				int q2 = (int) (Math.random() * randomNumberRoundValue);
				System.out.println("What is " + q1 + " * " + q2);
				int answer = input.nextInt();
				if (answer == (q1 * q2)) 
				{
					System.out.println("Awesome!. You made it great!!");
					totalMarks += 10;
				}
				else 
				{
					System.out.println("Sorry, wrong answer. Better luck for next question");
					totalMarks -= 10;
				}
				System.out.println("Press 'Y' for next question, else press 'q' to exit");
				String updatedGameStatus = input.next();
				gameStatus = updatedGameStatus.charAt(0);  // question // characterAt(2)=e
			}
			System.out.println("Total Marks are : " + totalMarks);

			break;

		case 4:  // 4--> Division
			while (gameStatus != 'q') 
			{
				int q1 = (int) (Math.random() * randomNumberRoundValue);
				int q2 = (int) (Math.random() * randomNumberRoundValue);
				System.out.println("What is " + q1 + " / " + q2);
				int answer = input.nextInt();
				if (answer == (q1 / q2))
				{
					System.out.println("Awesome!. You made it great!!");
					totalMarks += 10;
				} 
				else 
				{
					System.out.println("Sorry, wrong answer. Better luck for next question");
					totalMarks -= 10;
				}
				System.out.println("Press 'Y' for next question, else press 'q' to exit");
				String updatedGameStatus = input.next();
				gameStatus = updatedGameStatus.charAt(0);  // question // characterAt(2)=e
			}
			System.out.println("Total Marks are : " + totalMarks);

			break;
		default:
			System.out.println("Please select any one option the above from 1 to 4 :");
			break;
		}

	}
}

    
